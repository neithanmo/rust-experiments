extern crate rand;

use std::process;
use rand::Rng;
use std::env;
use std::collections::HashMap;

#[derive(Debug)]
struct Operations{

    size:u32,           // The number of numbers to generate for the items vector

    divisors:Vec<u32>,  // The vector of dividers 

    items: Vec<u32>,    // The vector of random numbers
}

impl Operations{
    fn new( args: &[String] ) -> Result<Operations, &'static str> {

        if args.len() <= 1 {

            return Err("Not enough arguments");
        }

        let size = args[1].clone().parse::<u32>().unwrap();
        
        // The arguments are string we need to cast to u32 and add each number into the
        // divisors list only if the casting operation was successful
        let divisors = args.iter().map(|x| x.parse::<u32>())
                                  .filter_map(Result::ok)
                                  .collect();
        // Random generator object
        let mut rng = rand::thread_rng();
        
        // Items vector which will contain the randomly generated numbers
        let items = vec![0; size as usize];
        // Iterates over the items vector and fill it with a random number between 1 and 10000
        let items = items.iter().map( |_| rng.gen_range::<u32>(1, 10000) ).collect();

        return Ok(Operations{size, divisors, items});

    }

    // Find the list of items which are divisible by each element in the divisors list
    fn find_divisible(&self) -> HashMap<u32, Vec<u32> >{

        let mut hash = HashMap::new();

        // goes trhough each divisor's elments
        for div in &self.divisors{

            // For each divisor verifies if not 0 and collect each item which is divisible
            // by it
            let result:Vec<u32> = self.items
                                .iter()
                                .filter( |&item| (*div != 0 && item % div == 0) )
                                .map(|&x| x)// map is required because filter generates a new &u32 iterator and we need a u32 one, maybe is not the best way
                                .collect();

            // Dictionary where the key is the divisor number and the value is 
            // a vector containing all numbers that are divisible by the divisor
            hash.insert(div.clone(), result);  
        }
        hash
    }
}

//fn fibonachi(size:&u32) -> Vec<u32>{
    //let fibonachi_serie = Vec::new();
    //fibonachi_serie
//}
fn main() {
    let args: Vec<String> = env::args().collect();

    let operations = Operations::new(&args).unwrap_or_else(|error| {
        eprintln!("Problem parsing arguments {}", error);
        process::exit(1);});
    println!("The operations struct is {:#?}", operations);
    println!("is divisible ? ");
    let divisible = operations.find_divisible();
    println!("result {:#?}", divisible);
}
